package view;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.BankAccount;

/**
   A frame that shows the growth of an investment with variable interest.
*/
public class InvestmentFrame extends JFrame
{  
   private static final int DEFAULT_RATE = 5;
private JLabel resultLabel; 
   private JPanel panel;
   private JLabel rateLabel;
   private JTextField rateField;
   private JButton button;
  
   
 
 
   public InvestmentFrame()
   {  
     
	  // Use instance variables for components 
      resultLabel = new JLabel("");
      // Use helper methods 
      createTextField();
      createButton();
      createPanel();
      
   }

   private void createTextField()
   {
      rateLabel = new JLabel("Interest Rate: ");
      final int FIELD_WIDTH = 10;
      rateField = new JTextField(FIELD_WIDTH);
      rateField.setText("" + DEFAULT_RATE);
   }
   
   private void createButton()
   {
      button = new JButton("Add Interest");
      
          }
      
  private void createPanel()
   {
      panel = new JPanel();
      panel.add(rateLabel);
      panel.add(rateField);
      panel.add(button);
      panel.add(resultLabel);      
      add(panel);
   }

public void setBalance(BankAccount account) {
	// TODO Auto-generated method stub
	 resultLabel.setText("balance: " + account.getBalance());
}

public String getrateField() {
	// TODO Auto-generated method stub
	return rateField.getText();
}

public void setListenner(ActionListener list) {
	// TODO Auto-generated method stub
	 button.addActionListener(list);
	
}


}